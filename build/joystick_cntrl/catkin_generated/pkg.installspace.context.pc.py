# generated from catkin/cmake/template/pkg.context.pc.in
CATKIN_PACKAGE_PREFIX = ""
PROJECT_PKG_CONFIG_INCLUDE_DIRS = "/home/sydney/MobRobProj/src/Project3/install/include".split(';') if "/home/sydney/MobRobProj/src/Project3/install/include" != "" else []
PROJECT_CATKIN_DEPENDS = "roscpp;rospy;std_msgs;geometry_msgs;turtlesim;sensor_msgs".replace(';', ' ')
PKG_CONFIG_LIBRARIES_WITH_PREFIX = "-lbeginner_tutorials".split(';') if "-lbeginner_tutorials" != "" else []
PROJECT_NAME = "beginner_tutorials"
PROJECT_SPACE_DIR = "/home/sydney/MobRobProj/src/Project3/install"
PROJECT_VERSION = "0.0.0"
