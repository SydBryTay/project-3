# generated from catkin/cmake/template/pkg.context.pc.in
CATKIN_PACKAGE_PREFIX = ""
PROJECT_PKG_CONFIG_INCLUDE_DIRS = "/home/sydney/MobRobProj/src/Project3/install/include".split(';') if "/home/sydney/MobRobProj/src/Project3/install/include" != "" else []
PROJECT_CATKIN_DEPENDS = "geometry_msgs;nav_msgs;roscpp;rospy;sensor_msgs;std_msgs;visualization_msgs;tf".replace(';', ' ')
PKG_CONFIG_LIBRARIES_WITH_PREFIX = "-lgoal2goal".split(';') if "-lgoal2goal" != "" else []
PROJECT_NAME = "go2goal"
PROJECT_SPACE_DIR = "/home/sydney/MobRobProj/src/Project3/install"
PROJECT_VERSION = "0.0.0"
