#!/bin/sh

if [ -n "$DESTDIR" ] ; then
    case $DESTDIR in
        /*) # ok
            ;;
        *)
            /bin/echo "DESTDIR argument must be absolute... "
            /bin/echo "otherwise python's distutils will bork things."
            exit 1
    esac
    DESTDIR_ARG="--root=$DESTDIR"
fi

echo_and_run() { echo "+ $@" ; "$@" ; }

echo_and_run cd "/home/sydney/MobRobProj/src/Project3/src/turtle/turtlebot3/turtlebot3_teleop"

# ensure that Python install destination exists
echo_and_run mkdir -p "$DESTDIR/home/sydney/MobRobProj/src/Project3/install/lib/python2.7/dist-packages"

# Note that PYTHONPATH is pulled from the environment to support installing
# into one location when some dependencies were installed in another
# location, #123.
echo_and_run /usr/bin/env \
    PYTHONPATH="/home/sydney/MobRobProj/src/Project3/install/lib/python2.7/dist-packages:/home/sydney/MobRobProj/src/Project3/build/lib/python2.7/dist-packages:$PYTHONPATH" \
    CATKIN_BINARY_DIR="/home/sydney/MobRobProj/src/Project3/build" \
    "/usr/bin/python2" \
    "/home/sydney/MobRobProj/src/Project3/src/turtle/turtlebot3/turtlebot3_teleop/setup.py" \
    build --build-base "/home/sydney/MobRobProj/src/Project3/build/turtle/turtlebot3/turtlebot3_teleop" \
    install \
    $DESTDIR_ARG \
    --install-layout=deb --prefix="/home/sydney/MobRobProj/src/Project3/install" --install-scripts="/home/sydney/MobRobProj/src/Project3/install/bin"
