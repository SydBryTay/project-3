# CMake generated Testfile for 
# Source directory: /home/sydney/MobRobProj/src/Project3/src
# Build directory: /home/sydney/MobRobProj/src/Project3/build
# 
# This file includes the relevant testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
subdirs("gtest")
subdirs("turtle/turtlebot3/turtlebot3")
subdirs("turtle/turtlebot_msgs")
subdirs("turtle/turtlebot3/turtlebot3_navigation")
subdirs("turtle/turtlebot3_sim/turtlebot3_simulations")
subdirs("low_level_control/controllers")
subdirs("low_level_control/dynamic_models")
subdirs("high_level_control/go2goal")
subdirs("turtle/turtlebot3/turtlebot3_bringup")
subdirs("turtle/turtlebot3/turtlebot3_example")
subdirs("turtle/turtlebot3_sim/turtlebot3_fake")
subdirs("turtle/turtlebot3_sim/turtlebot3_gazebo")
subdirs("turtle/turtlebot3/turtlebot3_slam")
subdirs("turtle/turtlebot3/turtlebot3_teleop")
subdirs("setup_and_launch/turtlebot_launch")
subdirs("low_level_control/turtlebot_sim")
subdirs("joystick_cntrl")
subdirs("turtle/turtlebot3/turtlebot3_description")
